import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, 
  MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, 
  MDBCard, MDBContainer, MDBAlert, MDBRow, MDBCol, 
  MDBInput, MDBBtn, MDBCardBody, MDBCardTitle } from 'mdbreact';
import axios from 'axios';
import MDSpinner from "react-md-spinner";
import { Redirect, Link, BrowserRouter as Router } from 'react-router-dom'
import { Pie, Doughnut } from "react-chartjs-2";
//import mongodb from 'mongodb'

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
     error: '',
     showErr: false,
     showScc: false,
     success: '',
     showLoader: false,
     dataPie: {},
     collapse: false,
     dataDoughnut: {}
    }  

    this.state.dataPie = {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
      datasets: [
        {
          data: [300, 50, 100, 40, 120, 24, 52],
          backgroundColor: [
            "#F7464A",
            "#46BFBD",
            "#FDB45C",
            "#949FB1",
            "#4D5360",
            "#ac64ad"
          ],
          hoverBackgroundColor: [
            "#FF5A5E",
            "#5AD3D1",
            "#FFC870",
            "#A8B3C5",
            "#616774",
            "#da92db"
          ]
        }
      ]
    }

    this.state.dataDoughnut = {
      labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
      datasets: [
        {
          data: [300, 50, 100, 40, 120],
          backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
          hoverBackgroundColor: [
            "#FF5A5E",
            "#5AD3D1",
            "#FFC870",
            "#A8B3C5",
            "#616774"
          ]
        }
      ]
    }
    this.onClick = this.onClick.bind(this);
   }

   onClick() {
    this.setState({
        collapse: !this.state.collapse,
      });
  }

  render() {
    const bgPink = {backgroundColor: '#e91e63'}
    const container = { height: 2300 }
    return (
      <div>
        <main>  
          <MDBContainer style={container} className="my-5">
          <MDBCard>
          <MDBCardBody cascade>
            <MDBCardTitle>Dashboard</MDBCardTitle>
            <MDBRow className="text-center">
              <MDBCol sm="6">
                <h3 className="mt-5">Students Data</h3>
                <Pie data={this.state.dataPie} options={{ responsive: true }} />
              </MDBCol>
              <MDBCol sm="6">
                <h3 className="mt-5">Accounts chart</h3>
                <Doughnut data={this.state.dataDoughnut} options={{ responsive: true }} />
              </MDBCol>
             </MDBRow> 
            </MDBCardBody>  
          </MDBCard>  
        </MDBContainer>
        </main>
      </div>  
    );
  }
}

export default Dashboard;
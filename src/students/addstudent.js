import React, { Component } from 'react';
import { MDBIcon, 
  MDBCard, MDBContainer, MDBAlert, MDBRow, MDBCol, 
  MDBInput, MDBBtn, MDBCardBody, MDBCardTitle } from 'mdbreact';
import axios from 'axios';
import MDSpinner from "react-md-spinner";
import { Redirect, Switch, Link, BrowserRouter as Router } from 'react-router-dom'

class AddStudent extends Component {
  constructor(props) {
    super(props)

    this.state = {
     error: '',
     showErr: false,
     showScc: false,
     success: '',
     showLoader: false,
     dataPie: {},
     collapse: false,
     dataDoughnut: {},
     toList: false
    }  
    this.onClick = this.onClick.bind(this);
    this.submitForm = this.submitForm.bind(this);
   }

   onClick() {
    this.setState({
        collapse: !this.state.collapse,
      });
  }

  submitForm(event) {    
      event.preventDefault();
      this.setState({showErr: false})

      this.setState({showLoader: true});
      console.log(this.state.toList);
      this.setState({toList: true});
  }

  render() {
    const bgPink = {backgroundColor: '#e91e63'}
    const container = { height: '100%' }

    if (this.state.toList === true) {
      return (
        <Router>
            <Redirect to='/students' push={true} />
          </Router>  
        )
    }
    return (
      <div>      
        <main>  
          <MDBContainer style={container} className="my-5">
          <MDBCard>
          <MDBCardBody cascade>
          {this.state.showErr ? <MDBAlert color="danger" > {this.state.error} </MDBAlert> : null}
          {this.state.showScc ? <MDBAlert color="success" > {this.state.success} </MDBAlert> : null}        
            <MDBCardTitle>Add Students Data</MDBCardTitle>
            <div>
              <Link to="/students">List Students</Link>            
            </div>
            
            <MDBRow className="text-center">
              <MDBCol sm="12">
              <form
                className="needs-validation"
                  onSubmit={this.submitForm}>
                <h4 className="mb-4">Student Details</h4>
                <MDBRow>
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student First Name"
                      group
                      type="text"
                      name="student_first_name"
                      required
                    />

                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Last Name"
                      group
                      type="text"
                      name="student_last_name"
                      required
                    />
                  </MDBCol>
                
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Class"
                      group
                      type="text"
                      name="student_class"
                      required
                    />

                  </MDBCol>
                </MDBRow>  
                <MDBRow> 
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student School Name"
                      group
                      type="text"
                      name="student_school_name"
                      required
                    />
                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Course"
                      group
                      type="text"
                      name="student_course"
                      required
                    />

                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Subjects"
                      group
                      type="text"
                      name="student_subjects"
                    />
                  </MDBCol>
                </MDBRow>

                <MDBRow>
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Email"
                      group
                      type="email"
                      validate
                      error="wrong"
                      success="right"
                      name="student_email"
                    />

                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Phone"
                      group
                      type="number"
                      validate
                      error="wrong"
                      success="right"
                      name="student_phone"
                      required
                      maxLength="10"
                      minLength="10"
                    />
                  </MDBCol>
                
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Address"
                      group
                      type="text"
                      validate
                      error="wrong"
                      success="right"
                      name="student_address"
                      required
                    />

                  </MDBCol>
                </MDBRow>

                <h4 className="mb-4">Additional Details</h4>

                <MDBRow>
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Father Name"
                      group
                      type="text"
                      validate
                      error="wrong"
                      success="right"
                      name="student_father_name"
                    />

                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Mother Name"
                      group
                      type="text"
                      validate
                      error="wrong"
                      success="right"
                      name="student_mother_name"
                    />
                  </MDBCol>
                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Sibling Name"
                      group
                      type="text"
                      validate
                      error="wrong"
                      success="right"
                      name="student_sibling_name"
                    />

                  </MDBCol>

                  <MDBCol sm="4">
                    <MDBInput
                      label="Student Sibling Class"
                      group
                      type="text"
                      validate
                      error="wrong"
                      success="right"
                      name="student_sibling_class"
                    />
                  </MDBCol>
                </MDBRow>

                <MDBRow>
                  <MDBCol>
                    <MDBBtn color="unique" type="submit">Save</MDBBtn>
                    {this.state.showLoader ? <MDSpinner></MDSpinner> : null}
                  </MDBCol>
                </MDBRow>
                </form>
              </MDBCol>
             </MDBRow> 
            </MDBCardBody>  
          </MDBCard>  
        </MDBContainer>
        </main>
      </div>  
    );
  }
}

export default AddStudent;
import React, { Component } from 'react';
import { MDBIcon, 
  MDBCard, MDBContainer, MDBAlert, MDBRow, MDBCol, 
  MDBInput, MDBBtn, MDBCardBody, MDBCardTitle } from 'mdbreact';
import axios from 'axios';
import MDSpinner from "react-md-spinner";
import { Redirect, Link, BrowserRouter as Router, withRouter } from 'react-router-dom';

class Students extends Component {
  constructor(props) {
    super(props)

    this.state = {
     error: '',
     showErr: false,
     showScc: false,
     success: '',
     showLoader: false,
     collapse: false     
    }  
    this.onClick = this.onClick.bind(this);    
   }

   onClick() {
    this.setState({
        collapse: !this.state.collapse,
      });
  }

  render() {
    const bgPink = {backgroundColor: '#e91e63'}
    const container = { height: '100%' }
    return (
      <div>
        <main>  
          <MDBContainer style={container} className="my-5">
          <MDBCard>
          <MDBCardBody cascade>
            <MDBCardTitle>List Students</MDBCardTitle>        
            <div>
              <Link to="/addstudent">Add Student</Link>            
            </div>
            <MDBRow className="text-center">
              <MDBCol sm="6">
              </MDBCol>
             </MDBRow> 
            </MDBCardBody>  
          </MDBCard>  
        </MDBContainer>
        </main>
      </div>  
    );
  }
}

export default withRouter(Students);
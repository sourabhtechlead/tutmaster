import React, { Component } from 'react';
import { MDBCard, MDBContainer, MDBAlert, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCardBody, MDBCardTitle } from 'mdbreact';
import axios from 'axios';
import MDSpinner from "react-md-spinner";
import { BrowserRouter, Redirect, Switch } from 'react-router-dom'


class Login extends Component {
  constructor(props) {
  super(props)

  this.state = {
   email: 'sourabhg@ustechsolutions.com',
   password: 'test@123',
   error: '',
   showErr: false,
   showScc: false,
   success: '',
   showLoader: false,
   toDashboard: false
  }

  this.handleChange = this.handleChange.bind(this)
  this.handleSubmit = this.handleSubmit.bind(this)
  this.handleFocus = this.handleFocus.bind(this)
 }

  handleSubmit (event) {
      event.preventDefault();
      this.setState({showErr: false})
      const user = {
        email: this.state.email,
        password: this.state.password
      };
      
      this.setState({showLoader: true});     

      this.setState({toDashboard: true});
      // axios.post(`http://lan.tutservices.com/login`, { user })
      // .then(res => {
      //   this.setState({showLoader: false});
      //   console.log(res);
      //   console.log(res.data);

      //   this.setState({showScc: true})
      //   this.setState({success: 'Success'})
      // })
      // .catch((e) => {
      //   this.setState({showLoader: false});
      //   this.setState({showErr: true})
      //   this.setState({error: e.message})
      // })  
  } 
  
  handleChange (event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleFocus (event) {
    this.setState({showErr: false})
    this.setState({showScc: false})
  }
  render() {
    if (this.state.toDashboard === true) {
      return (
        <Switch>
            <Redirect to='/dashboard' push={true} />
          </Switch>  
        )
    }

    return (
      <MDBContainer justify = "center">
      <MDBCard>
      <MDBCardBody cascade>
        <MDBCardTitle>Login</MDBCardTitle>
        {this.state.showErr ? <MDBAlert color="danger" > {this.state.error} </MDBAlert> : null}
        {this.state.showScc ? <MDBAlert color="success" > {this.state.success} </MDBAlert> : null}
        <form
          className="needs-validation"
          onSubmit={this.handleSubmit}>
          <MDBRow>
            <MDBCol md="4">
              <MDBInput
                label="Username"
                icon="envelope"
                name="email"
                group
                type="email"
                required
                value={this.state.email}
                onChange={this.handleChange}
                onFocus={this.handleFocus}
              />
              <div className="valid-feedback">Looks good!</div>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4">                
              <MDBInput
                label="Password"
                name="password"
                minLength="7"
                icon="lock"
                group
                type="password"
                required
                value={this.state.password}
                onChange={this.handleChange}
                onFocus={this.handleFocus}
              />

              <div className="valid-feedback">Looks good!</div>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4">
              <MDBBtn color="unique" type="submit">Login</MDBBtn>
              {this.state.showLoader ? <MDSpinner></MDSpinner> : null}
            </MDBCol>
          </MDBRow>  
          </form>
        </MDBCardBody>  
      </MDBCard>  
    </MDBContainer>
    );
  }
}

export default Login;
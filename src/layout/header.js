import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, 
  MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon, 
  MDBCard, MDBContainer, MDBAlert, MDBRow, MDBCol } from 'mdbreact';
import { Redirect, Link, BrowserRouter as Router } from 'react-router-dom'

class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }  
    this.onClick = this.onClick.bind(this);
   }

   onClick() {
    this.setState({
        collapse: !this.state.collapse,
      });
  }

  render() {
    const bgPink = {backgroundColor: '#e91e63'}
    return (     
      <div>
      <Router>
          <header>
            <MDBNavbar style={bgPink} dark expand="md">
              <MDBNavbarBrand href="/">
                  <strong>TutMaster</strong>
              </MDBNavbarBrand>
              <MDBNavbarToggler onClick={ this.onClick } />
              <MDBCollapse isOpen = { this.state.collapse } navbar>
                <MDBNavbarNav left>
                  <MDBNavItem>
                      <MDBNavLink to="/dashboard">Home</MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                      <MDBNavLink to="/students">Students</MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                      <MDBNavLink to="#">Accounts</MDBNavLink>
                  </MDBNavItem>
                </MDBNavbarNav>
              </MDBCollapse>
            </MDBNavbar>
          </header>
        </Router>
      </div>  
    );
  }
}

export default Header;
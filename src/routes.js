import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter } from "react-router-dom";
import Login from "./login/login";
import Dashboard from "./dashboard/dashboard";
import AddStudent from "./students/addstudent";
import Students from "./students/students";
import Header from "./layout/header";

class Routes extends Component {

  render() {
    return (
    <div>
    {window.location.pathname != "/" ? <Header /> : null}    
    <Router>
        <div>
            <Route exact path="/" component={withRouter(Login)} />
            <Route path="/dashboard" component={withRouter(Dashboard)} />
            <Route path="/students" component={withRouter(Students)} />
            <Route path="/addstudent" component={withRouter(AddStudent)} />
        </div>    
    </Router>
    </div>
    );
  }
}

export default Routes;